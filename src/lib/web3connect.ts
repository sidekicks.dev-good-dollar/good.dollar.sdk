import Web3Modal from 'web3modal'
import Web3 from 'web3'
import { Token } from "@uniswap/sdk-core";

import { tokenBalance } from "./network/utils/tokenBalance";
import { g$Price } from "./network/apollo";
import { cDaiPrice } from "./network/methods/cDaiPrice";
import * as tokens from "./network/tokens";
import * as staking from "./network/staking";
import * as ubi from "./network/ubi";
import * as buyG$ from "./network/buy";
import * as sellG$ from "./network/sell";
import { LIQUIDITY_PROTOCOL } from "./network/constants/protocols";
import { SupportedChainId } from "./network/constants/chains";
import { CDAI } from "./network/constants/tokens";

const web3Modal = new Web3Modal({
  cacheProvider: true,
  providerOptions: {},
  disableInjectedProvider: false,
})

export class Web3Connect {
  private provider?: any;
  private web3?: Web3;
  private account?: string;
  private readonly onAccountChange: (methods: Web3Methods) => void

  constructor(onAccountChange?: (methods: Web3Methods) => void) {
    if (!onAccountChange) {
      this.onAccountChange = () => void (0)
    } else {
      this.onAccountChange = onAccountChange
    }
  }

  public async connect() {
    try {
      this.provider = await web3Modal.connect()
    } catch (e) {
      console.log('Could not get a wallet connection', e)
      throw e
    }
    if (this.provider.on) {
      this.provider.on('accountsChanged', this._fetchAccountData.bind(this))
      this.provider.on('chainChanged', this._fetchAccountData.bind(this))
    }

    return this._fetchAccountData()
  }

  private async _fetchAccountData() {
    console.log(this.provider)
    this.web3 = new Web3(this.provider)

    console.log('Web3 instance is', this.web3, this.provider)

    const [accounts, chainId] = await Promise.all([
      this.web3.eth.getAccounts(),
      this.web3.eth.getChainId()
    ])

    console.log('Network ID', chainId)

    if (!accounts || accounts.length === 0) {
      throw new Error(`Can't get accounts list`)
    }

    console.log('Accounts list', accounts)
    this.account = accounts[0]
    console.log('Selected account', this.account)

    const methods = new Web3Methods(chainId, this.web3, this.account)

    this.onAccountChange(methods)

    return methods
  }
}

class Web3Methods {
  constructor(public chainId: number, public readonly web3: Web3, public readonly account: string) {
  }

  async test() {
    let _SELL_ = 0.000001

    let metaBuy = await buyG$.getMeta(this.web3, 'ETH', _SELL_)
    console.log(metaBuy?.outputAmount.toSignificant(6))

    if (metaBuy) {
      console.log(metaBuy)
      // await buyG$.approve(this.web3, metaBuy)
      // await buyG$.buy(this.web3, metaBuy)

      let metaBuyReverse = await buyG$.getMetaReverse(this.web3, 'ETH', metaBuy?.outputAmount.toExact())
      console.log(metaBuyReverse?.inputAmount.toSignificant(6), _SELL_.toString())
    }

    _SELL_ = _SELL_ * 1e5
    let metaSell = await sellG$.getMeta(this.web3, 'ETH', _SELL_)
    console.log(metaSell?.outputAmount.toSignificant(6))

    if (metaSell) {
      console.log(metaSell)
      await sellG$.approve(this.web3, metaSell)
      await sellG$.sell(this.web3, metaSell)

      let metaSellReverse = await sellG$.getMetaReverse(this.web3, 'ETH', metaSell?.outputAmount.toExact())
      console.log(metaSellReverse?.inputAmount.toSignificant(6), _SELL_.toString())
    }
  }

  async test2() {
    await staking.getTokenPriceInUSDC(this.web3, LIQUIDITY_PROTOCOL.COMPOUND, new Token(SupportedChainId.KOVAN, '0x4F96Fe3b7A6Cf9725f59d353F723c1bDb64CA6Aa', 18, 'DAI'))
    await staking.getTokenPriceInUSDC(this.web3, LIQUIDITY_PROTOCOL.COMPOUND, CDAI[this.chainId])
  }

  async g$price() {
    const { DAI: price } = await g$Price(this.chainId)
    return price
  }

  async cDaiRate() {
    return cDaiPrice(this.web3, this.chainId)
  }

  async allStakes() {
    return staking.getList(this.web3)
  }

  async allMyStakes() {
    return staking.getMyList(this.web3)
  }

  async stake() {
    return staking.stake(this.web3, '0xD034967a2Fd770eD109ab7e4897d26b68B89A176', 0.1, false)
  }

  async withdraw() {
    return staking.withdraw(this.web3, '0xD034967a2Fd770eD109ab7e4897d26b68B89A176', 50)
  }

  async claim() {
    return staking.claim(this.web3)
  }

  async getTokens() {
    await tokens.getList(this.chainId)
  }

  async balanceOf(token: Token | string) {
    return tokenBalance(this.web3, token, this.account)
  }

  async checkUBI() {
    return ubi.check(this.web3)
  }

  async claimUBI() {
    return ubi.claim(this.web3)
  }
}

import './style.css'
import 'regenerator-runtime/runtime'
import { Web3Connect } from './lib/web3connect'

let isConnected = false
let connection

document.getElementById('connect')!.addEventListener('click', async (e) => {
    if (!isConnected) {
      (e.target as HTMLButtonElement).innerHTML = 'Disconnect';
      connection = new Web3Connect(async (methods) => {
        console.log("Connected")

        await methods.test()
        // await methods.test2()
        // const ubi = await methods.checkUBI()
        // if (parseInt(ubi.toString()) > 0) {
        //   await methods.claimUBI()
        // }

        // await methods.allStakes()
        // await methods.stake()

        // const stake = await methods.allMyStakes()
        // console.log(stake[0].stake.amount.toSignificant(6))
        // console.log(stake[0].stake.amount$.toSignificant(6))

        // await methods.withdraw()
        // await methods.claim()

        // console.log('DAI', await methods.balanceOf('DAI').then(r => r.toSignificant(6)))

        //   await methods.balanceOf('ETH')
        //   await methods.balanceOf('WETH')
        //   await methods.balanceOf('DAI')
        //
        //   const [ETH, WETH, DAI, cDAI, G$, GDX, GDAO, UNI] = await Promise.all([
        //     methods.balanceOf('ETH'),
        //     methods.balanceOf('WETH'),
        //     methods.balanceOf('DAI'),
        //     methods.balanceOf('cDAI'),
        //     methods.balanceOf('G$'),
        //     methods.balanceOf('GDX'),
        //     methods.balanceOf('GDAO'),
        //     methods.balanceOf('UNI'),
        //   ])
        //
        //   console.log('ETH', ETH.toFixed(4))
        //   console.log('WETH', WETH.toFixed(4))
        //   console.log('DAI', DAI.toFixed(4))
        //   console.log('cDAI', cDAI.toFixed(4))
        //   console.log('G$', G$.toFixed(2))
        //   console.log('GDX', GDX.toFixed(2))
        //   console.log('GDAO', GDAO.toFixed(4))
        //   console.log('UNI', UNI.toFixed(4))
        //   console.log('------------')
        //   console.log('G$ Price', await methods.g$price().then(v => parseFloat(v).toFixed(6)), '$')
        //   console.log('cDai Rate', await methods.cDaiRate().then(v => v.toSignificant(6)))
      })

      await connection.connect()

      isConnected = true
    } else {
      (e.target as HTMLButtonElement).innerHTML = 'Connect';
      isConnected = false
    }
  }
)

// https://raw.githubusercontent.com/compound-finance/token-list/master/compound.tokenlist.json
// https://umaproject.org/uma.tokenlist.json
// https://raw.githubusercontent.com/SetProtocol/uniswap-tokenlist/main/set.tokenlist.json
// https://raw.githubusercontent.com/opynfinance/opyn-tokenlist/master/opyn-v1.tokenlist.json
// https://app.tryroll.com/tokens.json
// https://tokens.coingecko.com/uniswap/all.json
// https://www.gemini.com/uniswap/manifest.json
// https://raw.githubusercontent.com/The-Blockchain-Association/sec-notice-list/master/ba-sec-list.json
// https://raw.githubusercontent.com/Uniswap/mrkl-drop-data-chunks/final/chunks/mapping.json


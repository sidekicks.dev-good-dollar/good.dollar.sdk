# Docs

- `yarn dev` to run sandbox application.

## Links

- [Application Docs](https://docs.google.com/document/d/11lDeD3wQj2pmucnP4DTmJwTifA08zUg-QbmqZlP-EnE/edit)
- [Figma](https://www.figma.com/file/dAghYyBZjRv69mWxaBBXBf/good-dollar?node-id=0%3A1)


- [Uniswap v2](https://uniswap.org/docs/v2/)
- [Fuse network explorer](https://explorer.fuse.io/)
- [Fuse swap](https://fuseswap.com/#/swap)